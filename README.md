# Necro prode

## Herramientas
Este proyecto está hecho para conocer el funcionamiento de una aplicación basada en [Angular.JS ](https://angularjs.org/) para el funcionamiento general, [Bootstrap](http://getbootstrap.com/) para los estilos básicos del mismo, y el agregado de [Ani.JS](http://anijs.github.io/) para las animaciones

## Funcionamiento
Es un mini sistema que gestiona los posibles puntos de muerte de los personajes de [Game of Thrones](https://es.wikipedia.org/wiki/Game_of_Thrones) partiendo de la idea de que es una persona que:

* No ha visto la serie anteriormente ó
* Ha visto la serie y quiere hacer sus apuestas con los próximos episodios

## Historial de cambios
* **07-10-2015**: Agregado funcionamiento del módulo para ingresar y eliminar muertes, y refactorización.
* **06-10-2015**: Reorganización de la aplicación para utilizar la estructura propuesta por los profesores.
* **05-10-2015**: Agregado el repositorio con los componentes base del sistema.