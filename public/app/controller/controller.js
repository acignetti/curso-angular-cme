'use strict';

angular.module('Handlers', ['ngRoute']).
    value('characters', []).
    value('seasons', [
        {id: "", name: "All"},
        {id: 1, name: "First"},
        {id: 2, name: "Second"},
        {id: 3, name: "Third"},
        {id: 4, name: "Fourth"},
        {id: 5, name: "Fifth"},
    ]).
    value('episodes', [
        {id: "", value: "-"},
        {id: 1, value: "1"},
        {id: 2, value: "2"},
        {id: 3, value: "3"},
        {id: 4, value: "4"},
        {id: 5, value: "5"},
        {id: 6, value: "6"},
        {id: 7, value: "7"},
        {id: 8, value: "8"},
        {id: 9, value: "9"},
        {id: 10, value: "10"},
        {id: 11, value: "11"},
        {id: 12, value: "12"}
    ]).
    constant('names', 'Agustín Cignetti - Yamile Andrada').
    controller('home-handler', ['$scope', '$window', 'names',
        function ($scope, $window, names) {
            $scope.creators = names;

            // $scope.$watch(function(){
            //     return $window.innerHeight;
            // }, function(value) {
            //     $scope.height = 'height: ' + (value - 100) + 'px';
            // });

        }]).
    controller('detail-handler', ['$scope', '$routeParams', 'characters', 'wikiData',
        function ($scope, $routeParams, characters, wikiData) {
            angular.forEach(characters, function(value, key) {
                if ($routeParams.guess == value.id) {
                    $scope.element = value;
                    wikiData.getData(value.name).then(function(response) {
                        $scope.wikipedia = response;
                    });
                    
                }
            });
        }]).
    controller('guess-handler', ['$scope', 'characters', 'seasons', 'episodes',
        function ($scope, characters, seasons, episodes) {
            $scope.seasons = seasons;
            $scope.episodes = episodes;

            $scope.addGuess = function (guess) {
                guess.id = characters.length;
                characters.push(guess);
                $scope.goBack();
            }

            $scope.$on('clickedOnElement', function(event, element){
               $scope.element = element;
            });

            $scope.confirmDelete = function(id) {
                angular.forEach(characters, function(value, key) {
                    if (id == value.id) {
                        characters.splice(key, 1);
                    }
                });

                $('#deleteElement').modal('hide');
            }
        }]).
    controller('grid-handler', ['$scope', 'characters', 'seasons', 'episodes',
        function ($scope, characters, seasons, episodes) {
            $scope.characters = characters;
            $scope.seasons = seasons;
            $scope.episodes = episodes;

            $scope.showDelete = function(element) {
                $scope.$broadcast('clickedOnElement', element);
            }
        }]).
    run(['$rootScope', '$location', function ($rootScope, $location) {
        var history = [];

        $rootScope.$on('$routeChangeSuccess', function () {
            history.push($location.path());
        });

        $rootScope.goBack = function () {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : '/';
            $location.path(prevUrl);
        }
    }]);