'use strict';

angular.module('Services', ['ngRoute']).
    service('wikiData', ['$http', '$q', '$sce', function($http, $q, $sce){
        this.getData = function(name) {
            var defered = $q.defer();

            var url = 'http://en.wikipedia.org/w/api.php?titles=' + name + '&action=query&format=json&callback=JSON_CALLBACK&prop=extracts|pageimages&piprop=thumbnail&pithumbsize=350';

            $http.jsonp(url).then(function(response) {
                var page = response.data.query.pages;
                var response;

                angular.forEach(page, function(value){
                    if (typeof value.extract != 'undefined') {
                        var response = {
                            text: $sce.trustAsHtml(value.extract)
                        };

                        if (typeof value.thumbnail != 'undefined') {
                            response.thumbnail = value.thumbnail.source;
                        } else {
                            response.thumbnail = '';
                        }

                        defered.resolve(response);
                    }
                });

            }, function(response) {
                defered.reject(false);
            });
            
            return defered.promise;
        }
    }]);