'use strict';

angular.module('Config', ['ngRoute']).
    constant('VIEWS', {
        home: 'app/view/home.html',
        grid: 'app/view/grid.html',
        detail: 'app/view/details.html',
        guess: 'app/view/guess.html'
    }).
    config(['$routeProvider', 'VIEWS', function ($routeProvider, VIEWS) {
        $routeProvider.when('/', {
            controller: 'home-handler',
            templateUrl: VIEWS.home
        }).when('/detail/:guess', {
            controller: 'detail-handler',
            templateUrl: VIEWS.detail
        }).when('/add', {
            controller: 'guess-handler',
            templateUrl: VIEWS.guess
        }).when('/list', {
            controller: 'grid-handler',
            templateUrl: VIEWS.grid
        }).otherwise({
            redirectTo: '/'
        })
    }]);