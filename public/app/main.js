'use strict';

angular.module('main', [
    'ngRoute',
    'Config',
    'Handlers',
    'Services'
]);